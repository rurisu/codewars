const helloWorld = require("../helloWorld");

describe('properly prints "Hello World!"', () => {
  test("Creates a spy on console.log to verify that it was called with Hello World!", () => {
    const consoleSpy = jest.spyOn(console, "log");

    helloWorld();

    expect(consoleSpy).toHaveBeenCalledWith("Hello World!");
  });
});
