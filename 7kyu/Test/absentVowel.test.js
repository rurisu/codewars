const absentVowel = require("../absentVowel");

describe("Upon receiving a string, checks the presence of vowels returning the corresponding index ", () => {
  test("missing i vowel should return 2", () => {
    const index = absentVowel("Mak s a bg tuxedo cat");

    expect(index).toBe(2);
  });
});
