const toInteger = require("../toInteger");

describe("Convert any input received to integer", () => {
  test("convert decimal input to integer ", () => {
    const input = toInteger(13.46);
    expect(input).toBe(13);
  });

  test("convert null input to integer ", () => {
    const input = toInteger();
    expect(input).toBe(0);
  });

  test("convert negative input to integer ", () => {
    const input = toInteger(0);
    expect(input).toBe(0);
  });
});
