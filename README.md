# Codewars katas



## Jest 
The framework used for testing javascript functions

## npm run
As the script designated to run the tests

## Conclusion
To verify the way a function should work 
it's important to take into account every possible input/output a function manipulates, and to verify it works like it should we must test all the possibilities that it could encounter in order to leave no space for errors. 
The process of testing (and documenting) is very important in development because it makes the software's flow smoother, more secure and easier to understand.


