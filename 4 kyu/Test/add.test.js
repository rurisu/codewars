const add = require("../add");

describe("Should return the sum of 2 big numbers", () => {
  test("should return sum of 123+321 in string format", () => {
    const sum = add("123", "321");

    expect(sum).toBe("444");
  });
});
