const reverse = require("../reverseArray");

describe("Upon receiving an array, it should return it reversed", () => {
  test("given an array of strings should return it reversed", () => {
    var array = [];
    array = reverse(["first", "second", "third"]);

    expect(array).toStrictEqual(["third", "second", "first"]);
  });

  test("given an array of integers should return it reversed", () => {
    var array = [];
    array = reverse([1, 2, 3]);

    expect(array).toStrictEqual([3, 2, 1]);
  });

  test("given an empty array should return it empty", () => {
    var array = [];
    array = reverse([]);

    expect(array).toStrictEqual([]);
  });
});
