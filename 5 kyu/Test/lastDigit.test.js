const lastDigit = require("../lastDigit");

describe("Should return last decimal digit of a^b", () => {
  test("should return 9 as it is the last digit of 9^7 (4782969)", () => {
    const number = lastDigit("9", "7");
    expect(number).toBe(9);
  });
});
